const buildSimple = (graph) => {
    graph.addNode("start");
    graph.addNode("choose");
    graph.addNode("docks");
    graph.addNode("boat");
    graph.addNode("human guild");
    graph.addNode("elf guild");

    graph.addEdge("start", "choose");
    graph.addEdge("choose", "docks", { set: { race: 'human' }});
    graph.addEdge("choose", "docks", { set: { race: 'elf' }});
    graph.addEdge("docks", "boat");
    graph.addEdge("boat", "human guild", { require: { race: 'human' } } );
    graph.addEdge("boat", "elf guild", { require: { race: 'elf' } } );
}

const buildGrid = (graph) => {
    graph.addNode("start", {
        content: "You enter the game. You see a series of 3 buttons. Which one do you press?"
    });

    graph.addNode("a", {
        content: "You hear a buzz once the button is pressed. Which one will you press now?"
    });
    graph.addNode("b", {
        content: "The buttons light up as you press the next button. Choose again."
    });
    graph.addNode("c", {
        content: "After you press the third button you hear a clicking sound"
    });

    graph.addNode("correct", {
        content: "You entered the correct code. Good job!"
    });
    graph.addNode("incorrect", {
        content: "Something doesn't seem right. Maybe it was a bad code?"
    });

    graph.addEdge("start", "a", {text: "Press 1", exec:'a = 1' })
    graph.addEdge("start", "a", {text: "Press 2", exec:'a = 2' })
    graph.addEdge("start", "a", {text: "Press 3", exec:'a = 3' })

    graph.addEdge("a", "b", {text: "Press 1", exec: 'b = 1;'})
    graph.addEdge("a", "b", {text: "Press 2", exec: 'b = 2;'})
    graph.addEdge("a", "b", {text: "Press 3", exec: 'b = 3;'})

    graph.addEdge("b", "c", {text: "Press 1", exec: 'c = 1;'})
    graph.addEdge("b", "c", {text: "Press 2", exec: 'c = 2;'})
    graph.addEdge("b", "c", {text: "Press 3", exec: 'c = 3;'})

    graph.addEdge("c", "correct", {text: "Continue", cond: 'a==1 && b==2 && c==3'})
    graph.addEdge("c", "incorrect", {text: "Continue", cond: '!(a==1 && b==2 && c==3)'})
}

const buildGridSmall = (graph) => {
    graph.addNode("start", {
        content: "You enter the game. You see a series of 2 buttons. Which one do you press?"
    });

    graph.addNode("a", {
        content: "You hear a buzz once the button is pressed. Which one will you press now?"
    });
    graph.addNode("b", {
        content: "After you press the second button you hear a clicking sound"
    });

    graph.addNode("correct", {
        content: "You entered the correct code. Good job!"
    });
    graph.addNode("incorrect", {
        content: "Something doesn't seem right. Maybe it was a bad code?"
    });

    graph.addEdge("start", "a", {text: "Press 1", exec:'a = 1' })
    graph.addEdge("start", "a", {text: "Press 2", exec:'a = 2' })

    graph.addEdge("a", "b", {text: "Press 1", exec: 'b = 1;'})
    graph.addEdge("a", "b", {text: "Press 2", exec: 'b = 2;'})

    graph.addEdge("b", "correct", {text: "Continue", cond: 'a==1 && b==2'})
    graph.addEdge("b", "incorrect", {text: "Continue", cond: '!(a==1 && b==2)'})
}


const buildSmall = (graph) => {
    graph.addNode("start", {
        content: "You stand before three larg oaken doors. The first door appears to have a brass knob and is slighly stained brownish orange. The second door has a simple iron handle and appears to be unfinished. The third door is dark stained with a clay handle. What door do you open?"
    });
    graph.addNode("room", {
        content: "The door enters into a room that appears to be connected to all other doors. The room connects to a dark hallway"
    });
    graph.addNode("hallway", {
        content: "The hallway is just a deadend"
    });
    graph.addNode("secret", {
        content: "The hallways turns into a secret grove"
    });

    graph.addEdge("start", "room", {
        text: "Choose door 1",
        exec: 'door=1'
    });
    graph.addEdge("start", "room", {
        text: "Choose door 2",
        exec: 'door=2'
    });
    graph.addEdge("start", "room", {
        text: "Choose door 3",
        exec: 'door=3'
    });

    graph.addEdge("room", "hallway", {
        text: "Go down the hallway",
        cond: "(door === 1 || door === 2)"
    })
    graph.addEdge("room", "secret", {
        text: "Go down the hallway",
        cond: "(door === 3)"
    })

}
export {
    buildSimple,
    buildGrid,
    buildGridSmall,
    buildSmall
}
