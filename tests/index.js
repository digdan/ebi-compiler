import MultiGraph from '../src/libs/MultiGraph3';
import { buildGridSmall } from './build';

const graphStats = (graph) => {
    console.log('----------------------------')
    console.log('Nodes:', graph.nodes().length)
    console.log('Edges:', graph.edges().length)
}

const graph = new MultiGraph();
buildGridSmall(graph);
const newGraph = graph.process('start');

graphStats(graph);
graphStats(newGraph);