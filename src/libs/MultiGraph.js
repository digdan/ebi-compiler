import Graph from "graphology";
import md5 from 'md5';
import logger from "../logger";
import vm from "vm";
import { cloneDeep } from "lodash";

class MultiGraph extends Graph {
	constructor(props = {}) {
		super({
			...props,
			type: 'directed',
			multi: true
		});
		this.newGraph = new Graph({
			type: 'directed',
			multi: true
		});
		this.edgeLists = [];
	}

	contextSignature(context) {
		return md5(JSON.stringify(context)).substr(0, 16);
	}

	contentHash(content) {
		return md5(content);
	}

	terminalNodes() {
		for (let node of this.newGraph.nodeEntries()) {
			const outEdges = this.newGraph.outEdges(node[0]);
			if (outEdges.length === 0) {
				console.log('terminal', node);
			}
		}
	}

	process(start, defaultContext = {}, breadcrumb = [], contentcrumb = [], edgeList = []) {
		const context = defaultContext;
		const signature = this.contextSignature(context);
		const attributes = this.getNodeAttributes(start);
		const nodeContent = attributes.content || '';
		const contentHash = this.contentHash(`${nodeContent}-${JSON.stringify(this.edges(start))}`);
		this.newGraph.addNode(`${start}-${signature}`, {
			...attributes,
			contentHash,
			breadcrumb,
			contentcrumb
		});
		let foundEdges = 0;
		for (const edge of this.outEdgeEntries(start)) {
			if (edge[1].cond) {
				if (!vm.runInNewContext(edge[1].cond, context)) {
					continue; // Skip Condition
				}
			}
			if (edge[1].exec) {
				vm.runInNewContext(edge[1].exec, context);
			}
			const newContext = cloneDeep(context);
			const newSignature = this.contextSignature(newContext);
			foundEdges++;
			this.process(edge[3], newContext, [...breadcrumb, `${start}-${signature}`], [...contentcrumb, contentHash], [...edgeList, edge[0]]);
			this.newGraph.addEdge(`${start}-${signature}`, `${edge[3]}-${newSignature}`, { text: edge[1].text })
		}

		if (foundEdges === 0) {
			this.edgeLists.push(edgeList);
		} else {
			console.log('FDE', foundEdges);
		}
		if (breadcrumb.length === 0) {
			this.optimizeNewGraph();
			console.log('EL', this.edgeLists)
			return this.newGraph;
		}
	}

	optimizeNewGraph() {
		// Iterate through all nodes that have no exit edge.
		this.terminalNodes();

		// Build paths back to start into arrays
		// Match largest section of array.
		// Remove redundant content and link to a single unit.
	}
}

export default MultiGraph;
