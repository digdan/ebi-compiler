import Graph from "graphology";
import md5 from 'md5';
import vm from "vm";
import { cloneDeep } from "lodash";

class MultiGraph extends Graph {

	constructor(props = {}) {
		super({
			...props,
			type: 'directed',
			multi: true
		});
		this.newGraph = new Graph({
			type: 'directed',
			multi: true
		});
        this.contextMap = {};
        this.contentMap = {};
	}

	process(start, defaultContext = {}, inEdge = null, crumbs = { context:[], content:[]} ) {
		// start = entry 
		const context = defaultContext; // Context is key/vars 

		const signature = md5(JSON.stringify(context)); // Hash of context 
		this.contextMap[signature] = cloneDeep(context); // Associate context with hash in global store
		const attributes = this.getNodeAttributes(start); // Get entry node attributes
		const nodeContent = attributes.content || ''; // Build node content

		// Build list of Edges
		let edgeOutList = [];
		for (const edge of this.outEdgeEntries(start)) {
			if (edge[1].cond) {
				if (!vm.runInNewContext(edge[1].cond, context)) {
					continue; // Skip Condition
				}
			}
			edgeOutList.push(edge);
		}

		// Build Content hash based on content and edges
		const contentHash = md5(JSON.stringify({content: nodeContent, edges: edgeOutList}));

		// Check if content hash is unique or not
		if (this.contentMap[contentHash]) { // existing
			// no need to add this node, just link from/to it
		} else { // Otherwise back-fill to last non-unique content hash, or top level
			this.contextMap[signature].used = true;
			this.newGraph.addNode(`${start}-${contentHash}`, {
				...attributes,
				contentHash
			});
			// TODO BACK FILL NODES if needed
			console.log('new node', start, signature, contentHash, crumbs.context)
			crumbs.context.reverse().forEach( backContextCrumb => {
				const contextNameSplit = backContextCrumb.split('-'); // Get context hash from crumb
				const backContextHash = contextNameSplit.pop();
				const contextNodeName = contextNameSplit.join('-');
				console.log('check', backContextCrumb, this.contextMap)
				if (!this.newGraph.hasNode(`${contextNodeName}-${backContextHash}`)) {
					console.log('Need to backfill context', backContextHash, this.contextMap[backContextHash])
					this.newGraph.addNode(`${contextNodeName}-${backContextHash}`, {
						...attributes,
						contextHash: backContextHash
					});
		
				}
			})

		}

		this.contentMap[contentHash] = start;
		edgeOutList.forEach( edge => { // Iterate edges
			let newContext = cloneDeep(context);
			if (edge[1].exec) {
				vm.runInNewContext(edge[1].exec, newContext);
			}
			this.process(edge[3], newContext, edge, { context: [...crumbs.context, `${start}-${signature}`], content: [...crumbs.content, `${start}-${contentHash}`]});
		});

		if (crumbs.content.length === 0) { // Top level
			return this.newGraph;
		} else { // Build edges
			const lastContentCrumb = crumbs.content[crumbs.content.length - 1];
			const targetNode = `${start}-${contentHash}`;
			if (this.newGraph.hasEdge(lastContentCrumb, targetNode)) { // Edges already exist
				const existingEdges = this.newGraph.edges(lastContentCrumb, targetNode);
				const existingTexts = existingEdges.reduce( (acc, item) => { // Build an array of texts for these nodes
					const attributes = this.newGraph.getEdgeAttributes(item);
					acc.push(attributes.text);
					return acc;
				}, []);
				if (existingTexts.indexOf(inEdge[1].text) === -1) { // Is new text between these nodes
					this.newGraph.addEdge(lastContentCrumb, targetNode, {
						text: inEdge[1].text
					})	
				}
			} else { // Brand new edge 
				this.newGraph.addEdge(lastContentCrumb, targetNode, {
					text: inEdge[1].text
				})
			}
		}
	}
}

export default MultiGraph;
