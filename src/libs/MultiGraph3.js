import Graph from "graphology";
import md5 from 'md5';
import vm from "vm";
import { cloneDeep } from "lodash";

class MultiGraph extends Graph {

	constructor(props = {}) {
		super({
			...props,
			type: 'directed',
			multi: true
		});
		this.newGraph = new Graph({
			type: 'directed',
			multi: true
		});
        this.contextMap = {};
        this.contentMap = {};
	}

	tagNodes(start, props) {
		const { context={}, nodePath=[], edgePath=[], signaturePath=[] } = props;
		const signature = md5(this.getNodeAttribute(start, 'content'));		

		/**
		 * Build a list of out edges from this node.
		 * These nodes are conditional based on the edges `cond` property, which is a javascript condition
		 */		
		const edgeOutList = [...this.outEdgeEntries(start)].reduce( (acc, edge) => {
			if (edge[1].cond) {
				try {
					if (vm.runInNewContext(edge[1].cond, context)) {
						acc.push(edge);
					}	
				} catch (e) {
					console.log('Context evaluation error at edge', edge,' error :', e);
				}
			} else {
				acc.push(edge);
			}
			return acc;
		}, []);
		
		edgeOutList.forEach( edge => { // Iterate edges
			let newContext = cloneDeep(context);
			if (edge[1].exec) {
				vm.runInNewContext(edge[1].exec, newContext);
			}
			this.tagNodes(edge[3], {
				context: newContext,
				nodePath: [...nodePath, edge[3]],
				edgePath: [...edgePath, edge[0]], 
				signaturePath: [...signaturePath, signature]
			});
		});

		// load prevous node props
		const previousProps = this.getNodeAttributes(start);

		// Save props as attributes to node
		this.setNodeAttribute(start, 'contexts', (previousProps.contexts || []).concat(context));
		this.setNodeAttribute(start, 'nodePaths', (previousProps.nodePaths || []).concat([nodePath]));
		this.setNodeAttribute(start, 'edgePaths', (previousProps.edgePaths || []).concat([edgePath]));
		this.setNodeAttribute(start, 'signaturePaths', (previousProps.signaturePaths || []).concat([signaturePath]));
		this.setNodeAttribute(start, 'signature', signature);
		this.setNodeAttribute(start, 'exits', edgeOutList.length);
	}

	findExits() {
		let exits = [];
		for (const [node, attributes] of this.nodeEntries()) {
			if (attributes['exits'] === 0) {
				exits.push({
					node,
					attributes
				});
			}
		}
		return exits;
	}

	process(start) {
		this.tagNodes(start, {}); // Populate needed datas
		const exits = this.findExits();
		console.log('exits', JSON.stringify(exits, null, 4));
		// Render node recursivly, much like we did in tagNodes func.
		// Compare their current path to the edgePaths on the exits list.
		return this;
	}
}

export default MultiGraph;
