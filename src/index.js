import Epub from "epub-gen";
import fs from "fs";
import Queue from 'bull';

const processQueue = new Queue('process', 'redis://127.0.0.1:6379/2');


processQueue.process( (job, done) => {
	const option = {
		title: "Test title",
		author: "Danjelo Morgaux",
		publisher: "EBI",
		cover: "http://danmorgan.net/testimage.jpg",
		content: [
			{
				title: "About author",
				data: "Hello world"
			}
		]
	};
	new Epub(option, "../output/book.epub");
	done();
});
